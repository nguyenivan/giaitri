#!/usr/bin/python

"""Usage: python getflickr.py --model=elly-tran --albums=one-picece,red-bikini,red-dress,simple-buoi,underwear
REQUIRED:
  -m or --model
  -a or --album
"""

import sys
import flickrapi
from string import Template


api_key = '17e0ed4fe45b82b4350d4c6e401ea584'
api_secret = '3645313a454df62c'
email = 'appsvietnam.appspot.com@gmail.com'
password = 'nethouse'
user_id = '71085099@N02'
url_pattern = 'http://farm%(farm)s.staticflickr.com/%(server)s/%(id)s_%(secret)s_b.jpg'
celeb_template =Template("{ 'title': '$celeb','albums': {$albums} }")
album_template = Template("'$album':{'title':'$album','files':$files}")

def main(*argv):
	from getopt import getopt, GetoptError

	try:
		(opts, args) = getopt(argv[1:],\
							  'm:a:',\
							  ['model=', 'albums='])
	except GetoptError, e:
		print e
		print __doc__
		return 1
	
#	print opts
	if len(opts) != 2:
		print __doc__
		return 1	
	
	for o, a in opts:
		if o in ('-m', '--model'):
			model = a
		elif o in ('-a', '--albums'):
			albums = a	
	flickr = flickrapi.FlickrAPI(api_key,api_secret)
	flickr.token.path = 'token.txt'
	
	(token, frob) = flickr.get_token_part_one(perms='write')
	if not token: raw_input("Press ENTER after you authorized this program")
	flickr.get_token_part_two((token, frob))
	album_dict = ''
	for album in albums.split(','):
		print ('Processing model %s, album %s' % (model,album))
		tags = '%s,%s' % (model, album)
		urls = []
		for photo in flickr.walk(tag_mode='all', tags=tags, user_id = user_id):
			info = {'farm' : photo.get('farm'),
					'server' : photo.get('server'),
					'id' : photo.get('id'),
					'secret' : photo.get('secret'),
					}
			urls.append( url_pattern % info)
		album_dict = "%s%s," % ( album_dict , album_template.substitute(album=album, files=str(urls)) )
	celebs_dict = "collection['%s']=%s\n" % (model , celeb_template.substitute(celeb=model, albums=album_dict))
	print '*' * 80
	print
	print celebs_dict
	print
	print '*' * 80

if __name__ == '__main__':
	sys.exit(main(*sys.argv))
