#!/usr/bin/python

"""Usage: python syncflickr.py --dir=/Volumes/Data/models/go/bao-hoa
REQUIRED:
  -d or --dir
"""
import sys, flickrapi, os
from rename import get_collection
from exceptions import Exception

api_key = '17e0ed4fe45b82b4350d4c6e401ea584'
api_secret = '3645313a454df62c'
email = 'appsvietnam.appspot.com@gmail.com'
password = 'nethouse'
user_id = '71085099@N02'
url_pattern = 'http://farm%(farm)s.staticflickr.com/%(server)s/%(id)s_%(secret)s.jpg'


def sync_album(model, album, photos):
	current = ''
	def callback(progress, done):
		if done and current:
			print 'Uploaded %s' % current
			
	flickr = flickrapi.FlickrAPI(api_key,api_secret)
	(token, frob) = flickr.get_token_part_one(perms='write')
	if not token: raw_input("Press ENTER after you authorized this program")
	flickr.get_token_part_two((token, frob))
	
	tags = '%s,%s' % (model, album)
	print tags
	names = []
	for photo in photos:
		base_name = os.path.basename(photo)
		names.append(os.path.splitext(base_name)[0])
	titles = []
	to_remove = []
	skipped =[]
	for photo in flickr.walk(tag_mode='all', tags=tags, user_id = user_id):
		title = photo.get('title')
		id = photo.get('id')
		if title not in names:
			to_remove.append( '%s(%s)' % (title, id) )
		else:
			skipped.append( '%s(%s)' % (title, id) )
		titles.append(title)
	if skipped: print ('Skipped these photos %s' % str(skipped) )
	if to_remove: print ('You should manually remove these photos %s' % str(to_remove) )
	count = 0 
	added = []
	for name in names:
		try:
			if name not in titles:
				#upload photo
				current = photos[count]
				print 'Uploading %s' % current
				result = flickr.upload(filename=current, is_public='0', tags=tags, title=name)
				added.append ('%s(%s)' % (name, result.find('photoid').text) )
		except Exception, e:
			print 'Error %s while uploading file %s' % (e.message, current)
		count +=1
	if added: print ('You just added these photos %s' % str(added) ) 


def main(*argv):
	from getopt import getopt, GetoptError

	try:
		(opts, args) = getopt(argv[1:],\
							  'd:',\
							  ['dir='])
	except GetoptError, e:
		print e
		print __doc__
		return 1
	
	if len(opts) != 1:
		print __doc__
		return 1	
	
	for o, a in opts:
		if o in ('-d', '--dir'):
			dir = a	
			
	collection = get_collection(dir)
	for model in collection:
		for album in collection[model]:
			sync_album(model,album, collection[model][album])
		

if __name__ == '__main__':
	sys.exit(main(*sys.argv))
