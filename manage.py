#!/usr/bin/env python
import sys
sys.path[:0] =[ '/usr/local/google_appengine', 
			'/usr/local/google_appengine/lib/antlr3', 
			'/usr/local/google_appengine/lib/ipaddr',
			'/usr/local/google_appengine/lib/webob',
			'/usr/local/google_appengine/lib/fancy_urllib',
			'/usr/local/google_appengine/lib/yaml/lib',
			'/usr/local/google_appengine/lib/simplejson',
			'/usr/local/google_appengine/lib/webapp2',
			 ]
from django.core.management import execute_manager
try:
    import settings # Assumed to be in the same directory.
except ImportError:
    import sys
    sys.stderr.write("Error: Can't find the file 'settings.py' in the directory containing %r. It appears you've customized things.\nYou'll have to run django-admin.py, passing it your settings module.\n(If the file settings.py does indeed exist, it's causing an ImportError somehow.)\n" % __file__)
    sys.exit(1)
if __name__ == "__main__":
    execute_manager(settings)

