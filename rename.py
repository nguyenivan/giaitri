#!/usr/bin/env python
import os,sys, imghdr
import shutil
from exceptions import Exception
def urlify(s):
	import re
	# Remove all non-word characters (everything except numbers and letters)
	s = re.sub(r"\W", '_', s)
	# Replace all runs of whitespace with a single dash
	s = re.sub(r"[\s_]+", '-', s)
	return s

def rename_photos(dir):
	print "Processing directory %s:" % dir
	
	if os.path.isdir(dir):
		print "Removing hidden dirs from Picasa..."
		for dirname, dirnames, filenames in os.walk(dir):
			for folder in dirnames:
				if folder[:1] == ".":
					print "os remove %s" % folder
					shutil.rmtree(os.path.join(dirname, folder))
				else:
					os.rename(os.path.join(dirname, folder), os.path.join(dirname, urlify(folder)))
		print "Changing file names..."			
		for dirname, dirnames, filenames in os.walk(dir):
			count = 1
			for filename in filenames:
				ext = imghdr.what(os.path.join(dirname, filename))
				if ext:
					newname = '%s-%03d.%s' % ( urlify(os.path.split(dirname)[-1]) , count, os.path.splitext(filename)[1][1:])
					newname = os.path.join(dirname, newname)
					count += 1
					print  "os rename %s %s" % (os.path.join(dirname, filename), newname)
					os.rename(os.path.join(dirname, filename), newname)
	else:
		print "%s is not a directory. Please check and try again." % dir

def get_collection(dir):
	print "Processing directory %s:" % dir
	result = {}
	base_model = os.path.basename(dir)
	if os.path.isdir(dir):
		result[base_model] = {}
		for album in os.listdir(dir):
			base_album = album
			full_album = os.path.join(dir, base_album)
			if os.path.isdir(full_album) and base_album[:1]!='.' :
				result[base_model][base_album] = []
				for photo in os.listdir(full_album):
					full_photo = os.path.join(full_album, photo)
					if os.path.isfile(full_photo) and imghdr.what(full_photo):
						result[base_model][base_album].append(full_photo)
		return result
	else:
		raise Exception ("%s is not a directory. Please check and try again." % dir)
		
if __name__ == "__main__":
	if (len(sys.argv) == 2):
		rename_photos(sys.argv[1])
	else:
		print 'Systax: rename.py [dir]'
		print 'Rename all photos to format ngoc_trinh_nude01_001.jpg'
		