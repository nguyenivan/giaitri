from django.conf.urls.defaults import url, patterns
from django.views.generic.simple import redirect_to
import views

handler500 = 'djangotoolbox.errorviews.server_error'

urlpatterns = patterns('',
    ('^_ah/warmup$', 'djangoappengine.views.warmup'),
#    url(r'^proxy.pac$', 'django.views.generic.simple.direct_to_template',
#     {'template': 'proxy.pac'}, name="proxy"),
    url(r'^$', redirect_to , { 'url': '/celebrities/index.html', 'permanent': False}),
    url(r'^celebrities/index.html$', views.home),
    url(r'^celebrities/(?P<name>.*?)/(?P<mname>.*?).manifest$', views.manifest),	
    url(r'^celebrities/(?P<name>.*?)/(?P<album>.*?)/index.html$', views.photo),				
    url(r'^celebrities/(?P<name>.*?)/index.html$', views.celebrity),
    
    url(r'^$', redirect_to , { 'url': '/nguoi-mau/index.html', 'permanent': False}),
    url(r'^nguoi-mau/index.html$', views.home, name="home"),
    url(r'^nguoi-mau/(?P<name>.*?)/(?P<mname>.*?).manifest$', views.manifest , name="manifest"),	
    url(r'^nguoi-mau/(?P<name>.*?)/(?P<album>.*?)/index.html$', views.photo, name="photo"),				
    url(r'^nguoi-mau/(?P<name>.*?)/index.html$', views.celebrity, name="celebrity"),
)

