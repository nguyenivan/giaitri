#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os,sys, imghdr
from string import Template
import re

celeb_template =Template("{ 'title': '$celeb','albums': {$albums} }")
album_template = Template("'$album':{'title':'$album','files':$files}")

def gen_collection(dir):
	if os.path.isdir(dir):
		print "Processing directory %s:" % dir
		celebs = 'collection={}\n'
		for celeb in os.listdir(dir):
			celeb_dir = os.path.join(dir,celeb)
			if os.path.isdir(celeb_dir) and celeb[:1] != ".":
#				print 'Found celebrity %s' % celeb
				albums = ''
				for album in os.listdir(celeb_dir):
					album_dir = os.path.join(celeb_dir,album)
					if os.path.isdir(album_dir) and album[:1] != ".":
#						print '     -->Found album %s' % album
						files = {}
						for file in os.listdir(album_dir) :
							file_path = os.path.join(album_dir,file)
							if os.path.isfile(file_path) and  file[:1] != ".":
								name = os.path.splitext(file)[0]
								if re.match('^[a-zA-Z0-9-]+\d{3}$', name):
									base_name =  name[:-4]
									if files.get(base_name):
										files[base_name] +=1
									else:
										files[base_name] = 1
#						print files
						albums = "%s%s," % ( albums , album_template.substitute(album=album, files=str(files)) )
#					print albums
				celebs += "collection['%s']=%s\n" % (celeb , celeb_template.substitute(celeb=celeb, albums=albums))
		print celebs
	else:
		print "%s is not a directory. Please check and try again." % dir

if __name__ == "__main__":
	if (len(sys.argv) == 2):
		gen_collection(sys.argv[1])
	else:
		print 'Systax: collection.py [dir]'
		print 'Generate collection dictionary to use in views.py'
		